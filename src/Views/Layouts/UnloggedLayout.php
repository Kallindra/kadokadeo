<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title><?= $title ?></title>
	<link rel="stylesheet" href="/style.css" />
</head>
<body>
	<div id="loadFonts">
		<p style="font-family:Junegull-Regular;">.</p>
		<p style="font-family:Jost-Medium;">.</p>
	</div>
	<header id="topPage">
        <nav id="languageNav">
            <ul>
                <li><a href="#" title="Français"><img src="images/langFrench.gif" alt="french" title="Français"></a></li>
            </ul>
        </nav>
		<h1><span>KadoKadeo</span></h1>
	</header>
    <main id="container">
        <section id="bodySection">
	        <?= $mainContent ?>
        </section>
    </main>
</body>
</html>